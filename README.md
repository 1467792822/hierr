
# hierr

原来的名字为`hun_error`.  包装OS的错误码, 统一错误码的查询和获取接口

1. fn errno() -> i32;
2. fn `set_errno`(i32);
3. unsafe fn errmsg(i32) -> &str;

封装i32为Error

# Example
```rust
use hun_error::*;

let err = Error::last_error();
assert_eq!(err, Error::default());

set_errno(100);
let err = Error::last_error();
assert_eq!(err, 100.into());
```
